import React, { ReactElement } from 'react'
import * as snippet from '@segment/snippet'
import Document, { Html, Head, Main, NextScript, DocumentInitialProps } from 'next/document'
import { ServerStyleSheets } from '@material-ui/styles'

import theme from 'src/themes/theme'
import { RenderPageResult } from 'next/dist/next-server/lib/utils'

class MyDocument extends Document {
  // eslint-disable-next-line
  renderSnippet(): any {
    const opts = {
      apiKey: process.env.SEGMENT_WRITE_KEY,
      page: true
    }

    if (process.env.NODE_ENV === 'production') {
      return snippet.min(opts)
    }

    return snippet.max(opts)
  }

  render(): ReactElement {
    return (
      <Html lang={'en'} dir={'ltr'}>
        <Head>
          <meta charSet={'utf-8'} />
          <meta name={'description'} content={'Cloud Laboratory Information System'} />
          <meta name={'theme-color'} content={theme.palette.primary.main} />
          <meta property={'og:title'} content={'Dashlabs.ai'} />
          <meta property={'og:type'} content={'website'} />
          <meta property={'og:description'} content={'Cloud Laboratory Information System'} />
          <meta property={'og:url'} content={'https://dashlabs.app'} />
          <meta
            property={'og:image'}
            content={'https://storage.googleapis.com/dashlab_web_assets/url_preview_meta_image.png'}
          />
          <link
            rel={'shortcut icon'}
            type={'image/x-icon'}
            href={'https://storage.googleapis.com/dashlab_web_assets/DashlabsLettermarkLogoBlue.png'}
          />
          <link
            href={'https://storage.googleapis.com/dashlab_web_assets/DashlabsLogo-BlueBackground.png'}
            rel={'apple-touch-icon'}
          />
          <script dangerouslySetInnerHTML={{ __html: this.renderSnippet() }} />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

MyDocument.getInitialProps = async (ctx): Promise<DocumentInitialProps> => {
  const sheets = new ServerStyleSheets()
  const originalRenderPage = ctx.renderPage

  ctx.renderPage = (): RenderPageResult | Promise<RenderPageResult> =>
    originalRenderPage({
      enhanceApp: (App) => (props): ReactElement => sheets.collect(<App {...props} />)
    })

  const initialProps = await Document.getInitialProps(ctx)

  return {
    ...initialProps,
    styles: (
      <>
        {initialProps.styles}
        {sheets.getStyleElement()}
      </>
    )
  }
}

export default MyDocument
