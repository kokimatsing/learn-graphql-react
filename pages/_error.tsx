import React, { ReactElement } from 'react'
import { NextPage } from 'next'
import home from 'src/layouts/default'

const ErrorPage: NextPage = (): ReactElement => {
  return <>{'Something went wrong'}</>
}

export default home(ErrorPage)
