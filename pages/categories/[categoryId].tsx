import React, { ReactElement } from 'react'
import home from 'src/layouts/default'
import { NextPage } from 'next'

import PageTitle from 'src/components/_common/PageTitle'

import CategoriesUpdate from 'src/components/categories/Update'
import CategoriesDelete from 'src/components/categories/Delete'

const CategoryPage: NextPage = (): ReactElement => {
  return (
    <>
      <PageTitle title={'Category'} backButton />
      <CategoriesUpdate />
      <CategoriesDelete />
    </>
  )
}

export default home(CategoryPage)
