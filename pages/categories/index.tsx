import React, { ReactElement } from 'react'
import home from 'src/layouts/default'
import { NextPage } from 'next'

import PageTitle from 'src/components/_common/PageTitle'
import CategoriesCreate from 'src/components/categories/Create'
import CategoriesTable from 'src/components/categories/Table'

const CategoriesPage: NextPage = (): ReactElement => {
  return (
    <>
      <PageTitle title={'Categories'} />
      <CategoriesCreate />
      <CategoriesTable />
    </>
  )
}

export default home(CategoriesPage)
