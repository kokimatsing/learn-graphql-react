import React, { ReactElement } from 'react'
import { NextPage } from 'next'
import home from 'src/layouts/default'

const RootPage: NextPage = (): ReactElement => {
  return <>{'HomePage'}</>
}

export default home(RootPage)
