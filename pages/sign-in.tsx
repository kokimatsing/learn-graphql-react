import React, { ReactElement } from 'react'
import { NextPage } from 'next'
import home from 'src/layouts/default'
import UsersSignIn from 'src/components/users/SignIn'

const UsersSignInPage: NextPage = (): ReactElement => {
  return <UsersSignIn />
}

export default home(UsersSignInPage)
