import React, { ReactElement } from 'react'
import { NextPage } from 'next'
import home from 'src/layouts/default'

const NotFoundPage: NextPage = (): ReactElement => {
  return <>{'Page cannot be found'}</>
}

export default home(NotFoundPage)
