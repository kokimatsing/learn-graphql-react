import React, { ReactElement } from 'react'
import { NextPage } from 'next'
import home from 'src/layouts/default'
import UsersCreate from 'src/components/users/Create'

const UsersCreatePage: NextPage = (): ReactElement => {
  return <UsersCreate />
}

export default home(UsersCreatePage)
