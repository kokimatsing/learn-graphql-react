import React, { ReactElement } from 'react'
import Router from 'next/router'

import App from 'next/app'
import Head from 'next/head'

import ThemeProvider from '@material-ui/styles/ThemeProvider'
import CssBaseline from '@material-ui/core/CssBaseline'

import darkTheme from 'src/themes/darkTheme'
import theme from 'src/themes/theme'

import { ApolloProvider } from '@apollo/client'
import apolloClient from 'src/_utils/client'

import Notification from 'src/components/_common/Notification'
import SetCoordinates from 'src/components/SetCoordinates'

declare global {
  namespace NodeJS {
    interface Global {
      lat: number
      lng: number
      toggleDarkTheme: VoidFunction
      darkTheme: boolean
      setNotification: (type: string, message: string) => void
    }
  }
}

declare global {
  interface Window {
    // eslint-disable-next-line
    analytics: any
    // eslint-disable-next-line
    intercomSettings: any
  }
}

Router.events.on('routeChangeComplete', (url) => {
  if (process.browser) {
    window.analytics.page(url)
  }
})

class MyApp extends App {
  state: {
    darkTheme: false
  }

  render(): ReactElement {
    const { Component, pageProps } = this.props

    global.toggleDarkTheme = (): void => {
      if (process.browser) {
        window.localStorage.setItem(
          'enableDarkTheme',
          window.localStorage.enableDarkTheme === 'true' ? 'false' : 'true'
        )
      }
      this.setState({ darkTheme: !this.state.darkTheme })
      global.darkTheme = !this.state.darkTheme
    }

    return (
      <>
        <Head>
          <title>{'Dashlabs.ai'}</title>
        </Head>
        <ThemeProvider theme={global.darkTheme ? darkTheme : theme}>
          <CssBaseline />
          <ApolloProvider client={apolloClient}>
            <Component {...pageProps} />
            <Notification />
            <SetCoordinates />
          </ApolloProvider>
        </ThemeProvider>
      </>
    )
  }
}

export default MyApp
