import React, { ReactElement } from 'react'
import home from 'src/layouts/default'
import { NextPage } from 'next'

import PageTitle from 'src/components/_common/PageTitle'
import MoviesDetails from 'src/components/movies/Details'

const CategoriesPage: NextPage = (): ReactElement => {
  return (
    <>
      <PageTitle title={'Movie Details'} />
      <MoviesDetails />
    </>
  )
}

export default home(CategoriesPage)
