import React, { ReactElement } from 'react'
import home from 'src/layouts/default'
import { NextPage } from 'next'

import PageTitle from 'src/components/_common/PageTitle'
import MoviesUpdate from 'src/components/movies/Update'

const CategoriesPage: NextPage = (): ReactElement => {
  return (
    <>
      <PageTitle title={'Movie Update'} />
      <MoviesUpdate />
    </>
  )
}

export default home(CategoriesPage)
