import React, { ReactElement } from 'react'
import home from 'src/layouts/default'
import { NextPage } from 'next'

import PageTitle from 'src/components/_common/PageTitle'
import MoviesDelete from 'src/components/movies/Delete'

const CategoriesPage: NextPage = (): ReactElement => {
  return (
    <>
      <PageTitle title={'Movie Delete'} />
      <MoviesDelete />
    </>
  )
}

export default home(CategoriesPage)
