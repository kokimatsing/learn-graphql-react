import React, { ReactElement } from 'react'
import home from 'src/layouts/default'
import { NextPage } from 'next'

import PageTitle from 'src/components/_common/PageTitle'
import MoviesCreate from 'src/components/movies/Create'

const CategoriesPage: NextPage = (): ReactElement => {
  return (
    <>
      <PageTitle title={'Create Movie'} />
      <MoviesCreate />
    </>
  )
}

export default home(CategoriesPage)
