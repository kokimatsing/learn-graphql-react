const isProduction = process.env.NODE_ENV === 'production'

const SEGMENT_WRITE_KEY = isProduction
  ? 'nXDUNYHQ7AwtVXofEdgrdXEqomzmRfLZ'
  : 'uAyhylJhvAecDLhMRw5ryUJeiMgVOooE'

const { createSecureHeaders } = require("next-secure-headers");

const contentSecurityPolicy = {
  directives: {
    defaultSrc: [
      "'self'",
      'blob:',
      'https://test-sources.paymongo.com'
    ],
    fontSrc: ["'self'", 'data:', 'https:'],
    imgSrc: [
      '*.dashlabs.xyz:*',
      'localhost:*',
      "'self'",
      'data:',
      'https:',
      'blob:'
    ],
    connectSrc: [
      '*.dashlabs.xyz:*',
      'api-iam.intercom.io',
      'api.segment.io',
      'blob:',
      '*.dashlabs.app',
      'https://api.paymongo.com/',
      'localhost:*',
      "'self'"
    ],
    scriptSrc: [
      'api-iam.intercom.io',
      'blob:',
      'https://www.google-analytics.com/analytics.js',
      'js.intercomcdn.com',
      'widget.intercom.io',
      'https://www.googletagmanager.com/gtag/js',
      'www.gstatic.com',
      "'self'",
      "'unsafe-eval'",
      "'unsafe-inline'",
      `https://cdn.segment.com/analytics.js/v1/${SEGMENT_WRITE_KEY}/analytics.min.js`,
    ],
    styleSrc: [
      '*.googleapis.com',
      'www.gstatic.com', 
      "'self'", 
      "'unsafe-inline'"
    ]
  }
}

module.exports = {
  target: 'serverless',
  poweredByHeader: false,
  webpack(config) {
    config.resolve.modules.push(__dirname)
    return config
  },
  async headers() {
    return [{
      source: "/(.*)",
      headers: [
        ...(createSecureHeaders({
          contentSecurityPolicy,
          forceHTTPSRedirect: [true, { maxAge: 31536000, includeSubDomains: true, preload: true }],
          referrerPolicy: "same-origin",
          frameGuard: "deny",
          xssProtection: "block-rendering"
        })),
        {
          key: "permissions-policy",
          value: "geolocation=(self)"
        }
      ],
    }];
  },
  env: {
    SEGMENT_WRITE_KEY
  }
}
