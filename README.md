# Introduction

Learning GraphQL in frontend is easier compare to frontend and it's more about learning react. We're using Nextjs as react framework. 

This guide will follow the convention and structure of the production codebase in Dashlabs. Some concept are too long to discuss here but links for the documentation will be provided.

This guide is divided into several parts and each part has corresponding branch in the repo. All code changes will not be displayed here but you can checkout the branch to see changes of the mentioned file.

# PART 1 - Setup

This is a standard `nextjs` setup. 

File Structure:
```
pages
public
src
    _types
    _utils
    components
    layouts
    themes
```

# Part 2 - User Access

### Registration

Create `pages/register.tsx`

Create Users `Create` component

```
src
    components
        users
            Create
                index.tsx
                mutation.ts
```

`src/components/users/Create/index.tsx`

`src/components/users/Create/mutation.ts`

Now browse `http://localhost:3031/register` and register a new user.

### Sign In

Create `pages/sign-in.tsx`

Create Users `SignIn` component

```
src
    components
        users
            SignIn
                index.tsx
                mutation.ts
```

After successful login, `authToken` will be saved in cookie and user will be redirected to homepage.
```
// src\layouts\default\index.tsx
```

Now browse `http://localhost:3031/sign-in` and authenticate a user.

# Part 3 - Category CRUD

### Display Categories in a table

Create new page `pages/categories/index.tsx`

Add new component to display all categories in a table `src/components/categories/Table`

```
src
    components
        categories
            Table
                index.tsx
                query.ts
```

### Create category

Update `pages/categories/index.tsx` to include the category form. 

Create the category form component `
```
src
    components
        categories
            Create
                index.tsx
                mutation.ts
            Table
```

### Update Category

Create new page `pages/categories/[categoryId].tsx`
```
import React, { ReactElement } from 'react'
import home from 'src/layouts/default'
import { NextPage } from 'next'

import PageTitle from 'src/components/_common/PageTitle'
import CategoriesUpdate from 'src/components/categories/Update'

const CategoryPage: NextPage = (): ReactElement => {
  return (
    <>
      <PageTitle title={'Category'} backButton />
      <CategoriesUpdate />
    </>
  )
}

export default home(CategoryPage)
```

Create the update category form component `
```
src
    components
        categories
            Create
            Table
            Update
                index.tsx
                mutation.ts
                query.ts
```

### Delete Category

Update page `pages/categories/[categoryId].tsx` to add the delete form

Create the delete category form component `
```
src
    components
        categories
            Create
            Delete
                index.tsx
                mutation.ts
                query.ts
            Table
            Update
```

### Checking authorization

add query in Create category component
```
// src/components/categories/Create/query.ts
import { gql } from '@apollo/client'

export default gql`
  query {
    user: current_user {
      _id
      name
      userAuthorizations{
        scope
        read
        write
        destroy
      }
    }
  }
`

```

update `src/components/categories/Create/index.tsx` to add the `query`

```
...
import { User } from '../../../_types/users'
import { UserAuthorization } from '../../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../../_types/enums/userAuthorizationScope'
...
  const { data } = useQuery(query, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
    partialRefetch: true,
    returnPartialData: true,
    ssr: false
  })

  const user: User = data?.user

  const categoryAuthorization: UserAuthorization = user?.userAuthorization?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.CATEGORY
  })

  if (!categoryAuthorization?.write) {
    return null
  }
  ...
```

Now try to implement this in `Update` and `Delete` components.

# Part 4 - Category CRUD

### Create Movie

update `src\layouts\default\MyAppBar.tsx` to add button linked to create page

create new page `pages\movies\create.tsx`

create new component `components/movies/Create`

```
src
    components
        movies
            Create
                index.tsx
                mutation.tsx
                query.tsx
```

### Display movie details

create new page `pages\movies\[movieId]\index.tsx`

then create new component `Details` in movies

```
src
    components
        movies
            Create
            Details
                index.tsx
                query.tsx
```

### Edit movie

create new page `pages\movies\[movieId]\edit.tsx`

then create new component `Update` in movies

```
src
    components
        movies
            Create
            Details
            Update
                index.tsx
                mutation.ts
                query.tsx
```

### Delete a movie

create new page `pages\movies\[movieId]\delete.tsx`

then create new component `Delete` in movies

```
src
    components
        movies
            Create
            Delete
                index.tsx
                mutation.ts
                query.tsx
            Details
            Update
```