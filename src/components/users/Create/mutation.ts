import { gql } from '@apollo/client'

export default gql`
  mutation(
    $email: String!
    $name: String!
    $password: String!
  ) {
    user: create_user(
      email: $email
      name: $name
      password: $password
    ) {
      _id
      name
      email
    }
  }
`
