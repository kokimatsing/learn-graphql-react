import Button from '@material-ui/core/Button'
import CardContainer from 'src/components/_common/CardContainer'
import React, { ReactElement, useState } from 'react'
import TextField from '@material-ui/core/TextField'

import * as EmailValidator from 'email-validator'

import mutation from './mutation'
import { useMutation, ApolloError } from '@apollo/client'
import { CreateUserArgs } from '../../../_types/users'
import { useRouter } from 'next/router'

const UsersCreate = (): ReactElement => {
  const router = useRouter()

  const [email, setEmail] = useState<string>('')
  const [emailError, setEmailError] = useState(false)

  const [name, setName] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const createVariables: CreateUserArgs = {
    email,
    name,
    password
  }

  const [create, mutationState] = useMutation(mutation, {
    variables: createVariables,
    onCompleted: (): void => {
      global.setNotification('success', 'Registration success. Please login.')
      router.push('/sign-in')
    },
    onError: (error: ApolloError): void => {
      global.setNotification('error', error.graphQLErrors.map((e) => e.message).join(''))
    }
  })

  const submitDisabled = mutationState.loading || name === '' || email === '' || password === '' || emailError

  return (
    <>
      <CardContainer
        title={'Create account'}
        loading={mutationState.loading}
        content={
          <>
            <TextField
              size={'small'}
              variant={'outlined'}
              fullWidth
              margin={'dense'}
              label={'Name'}
              value={name}
              onChange={(e): void => {
                setName(e.target.value)
              }}
            />
            <TextField
              size={'small'}
              variant={'outlined'}
              fullWidth
              margin={'dense'}
              label={'Email'}
              value={email}
              onChange={(e): void => {
                setEmail(e.target.value)
              }}
              error={emailError}
              helperText={emailError && 'Invalid email'}
              onBlur={(): void => {
                const invalidEmail = email.length > 0 && !EmailValidator.validate(email)
                setEmailError(invalidEmail)
              }}
            />
            <TextField
              size={'small'}
              variant={'outlined'}
              fullWidth
              margin={'dense'}
              label={'Password'}
              value={password}
              type={'password'}
              onChange={(e): void => {
                setPassword(e.target.value)
              }}
            />
          </>
        }
        actions={
          <>
            <Button
              variant={'outlined'}
              onClick={(): void => {
                create()
              }}
              disabled={submitDisabled}
              color={'primary'}
            >
              {'Submit'}
            </Button>
            <Button
              onClick={(): void => {
                router.push('/sign-in')
              }}
            >
              {'Sign In'}
            </Button>
          </>
        }
      />
    </>
  )
}

export default UsersCreate
