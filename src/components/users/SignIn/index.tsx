import Button from '@material-ui/core/Button'
import CardContainer from 'src/components/_common/CardContainer'
import React, { ReactElement, useState } from 'react'
import TextField from '@material-ui/core/TextField'

import mutation from './mutation'
import { useMutation, ApolloError } from '@apollo/client'
import { CreateUserArgs, UserToken } from '../../../_types/users'
import { useRouter } from 'next/router'
import saveAccessToken from 'src/_utils/saveAccessToken'

const UsersCreate = (): ReactElement => {
  const router = useRouter()

  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const createVariables: CreateUserArgs = {
    email,
    password
  }

  const [create, mutationState] = useMutation(mutation, {
    variables: createVariables,
    onCompleted: async (e: { userToken: UserToken }): Promise<void> => {
      const userToken: UserToken = e.userToken

      saveAccessToken(userToken.authToken)

      if (userToken.authToken) {
        global.setNotification('success', 'Sign in successful')
        router.push('/')
        return
      }
    },
    onError: (error: ApolloError): void => {
      global.setNotification('error', error.graphQLErrors.map((e) => e.message).join(''))
    }
  })

  const submitDisabled = mutationState.loading || email === '' || password === ''

  return (
    <>
      <CardContainer
        title={'Sign in'}
        loading={mutationState.loading}
        content={
          <>
            <TextField
              size={'small'}
              variant={'outlined'}
              fullWidth
              margin={'dense'}
              label={'Email'}
              value={email}
              onChange={(e): void => {
                setEmail(e.target.value)
              }}
            />
            <TextField
              size={'small'}
              variant={'outlined'}
              fullWidth
              margin={'dense'}
              label={'Password'}
              value={password}
              type={'password'}
              onChange={(e): void => {
                setPassword(e.target.value)
              }}
            />
          </>
        }
        actions={
          <>
            <Button
              variant={'outlined'}
              onClick={(): void => {
                create()
              }}
              disabled={submitDisabled}
              color={'primary'}
            >
              {'Submit'}
            </Button>
            <Button
              onClick={(): void => {
                router.push('/register')
              }}
            >
              {'Register'}
            </Button>
          </>
        }
      />
    </>
  )
}

export default UsersCreate
