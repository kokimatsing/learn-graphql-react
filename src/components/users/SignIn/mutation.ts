import { gql } from '@apollo/client'

export default gql`
  mutation(
    $email: String!
    $password: String!
  ) {
    userToken: sign_in_user(
      email: $email
      password: $password
    ) {
      _id
      name
      email
      authToken
    }
  }
`
