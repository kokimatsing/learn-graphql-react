import React, { ReactElement, useState } from 'react'
import { useRouter } from 'next/router'

import CardContainer from 'src/components/_common/CardContainer'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import { useQuery, useMutation, ApolloError } from '@apollo/client'
import mutation from './mutation'
import query from './query'

import { Movie, MovieArgs, DeleteMovieArgs } from '../../../_types/movies'
import { User } from '../../../_types/users'
import { UserAuthorization } from '../../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../../_types/enums/userAuthorizationScope'

const CategoriesDelete = (): ReactElement => {
  const router = useRouter()
  const movieId = router?.query?.movieId as string

  const [checked, setChecked] = useState<boolean>(false)

  const queryVariables: MovieArgs = {
    _id: movieId
  }

  const { data, loading } = useQuery(query, {
    skip: !process.browser || !movieId,
    variables: queryVariables
  })

  const movie: Movie = data?.movie
  const user: User = data?.user

  const movieAuthorization: UserAuthorization = user?.userAuthorizations?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.MOVIE
  })

  const deleteVariables: DeleteMovieArgs = {
    _id: movieId
  }

  const [deleteTemplate, mutationState] = useMutation(mutation, {
    variables: deleteVariables,
    onCompleted: (): void => {
      global.setNotification('success', 'Delete successful')
      router.push('/', '/')
    },
    onError: (e: ApolloError): void => {
      global.setNotification('error', e.message)
    }
  })

  return movieAuthorization?.destroy ? (
    <>
      <form
        onSubmit={(e): void => {
          e.preventDefault()
        }}
      >
        <CardContainer
          title={`Delete ${movie?.name}`}
          loading={loading || mutationState.loading}
          content={
            <>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checked}
                    onChange={(): void => {
                      setChecked(!checked)
                    }}
                    color={'primary'}
                  />
                }
                label={'Confirm I want to delete this movie'}
              />
            </>
          }
          actions={
            <Button
              type={'submit'}
              size={'small'}
              color={'primary'}
              variant={'contained'}
              disabled={!checked}
              onClick={(): void => {
                deleteTemplate()
              }}
            >
              {'Delete'}
            </Button>
          }
        />
      </form>
    </>
  ) : null
}

export default CategoriesDelete
