import { gql } from '@apollo/client'

export default gql`
  mutation(
    $_id: ID!
  ) {
    movie: delete_movie(
      _id: $_id
    ) {
      _id
    }
  }
`
