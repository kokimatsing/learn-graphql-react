import React, { ReactElement } from 'react'
import { useRouter } from 'next/router'

import CardContainer from 'src/components/_common/CardContainer'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { useQuery } from '@apollo/client'
import query from './query'

import { Movie, MovieArgs } from '../../../_types/movies'
import { User } from '../../../_types/users'
import { UserAuthorization } from '../../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../../_types/enums/userAuthorizationScope'

const MoviesDetails = (): ReactElement => {
  const router = useRouter()
  const movieId = router?.query?.movieId as string

  const queryVariables: MovieArgs = {
    _id: movieId
  }

  const { data, loading } = useQuery(query, {
    skip: !process.browser || !movieId,
    variables: queryVariables
  })

  const movie: Movie = data?.movie
  const user: User = data?.user

  const movieAuthorization: UserAuthorization = user?.userAuthorizations?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.MOVIE
  })

  console.log(movieAuthorization)

  return (
    <>
      <CardContainer
        title={movie?.name}
        subheader={`Category: ${movie?.category?.name || 'n/a'}, Year: ${movie?.year || 'n/a'}`}
        loading={loading}
        content={
          <>
            <Typography variant={'body1'}>{movie?.description}</Typography>
          </>
        }
        actions={
          <>
            {movieAuthorization?.write ? (
              <>
                <Button
                  disabled={loading}
                  variant={'outlined'}
                  color={'primary'}
                  onClick={(): void => {
                    router.push(`/movies/${movie?._id}/update`)
                  }}
                >
                  {'Edit'}
                </Button>
              </>
            ) : null}

            {movieAuthorization?.destroy ? (
              <>
                <Button
                  disabled={loading}
                  variant={'outlined'}
                  color={'default'}
                  onClick={(): void => {
                    router.push(`/movies/${movie?._id}/delete`)
                  }}
                >
                  {'Delete'}
                </Button>
              </>
            ) : null}
          </>
        }
      />
    </>
  )
}

export default MoviesDetails
