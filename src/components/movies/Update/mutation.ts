import { gql } from '@apollo/client'

export default gql`
  mutation(
    $_id: ID!
    $name: String
    $description: String
    $year: Int
    $categoryId: String
  ) {
    movie: update_movie(
      _id: $_id
      name: $name
      description: $description
      year: $year
      categoryId: $categoryId
    ) {
      _id
      name
      description
      year
      categoryId
    }
  }
`
