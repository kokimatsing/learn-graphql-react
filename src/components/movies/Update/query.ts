import { gql } from '@apollo/client'

export default gql`
  query($_id: ID!) {
    movie: movie(
      _id: $_id
    ) {
      _id
      name
      description
      year
      categoryId
    }
    user: current_user {
      _id
      name
      userAuthorizations {
        scope
        read
        write
        destroy
      }
    }
  }
`
