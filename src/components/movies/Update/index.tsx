import React, { ReactElement, useState } from 'react'
import { useRouter } from 'next/router'

import CardContainer from 'src/components/_common/CardContainer'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import { useMutation, useQuery, ApolloError } from '@apollo/client'
import mutation from './mutation'
import query from './query'

import { Movie, MovieArgs, UpdateMovieArgs } from '../../../_types/movies'
import { User } from '../../../_types/users'
import { UserAuthorization } from '../../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../../_types/enums/userAuthorizationScope'

const MoviesUpdate = (): ReactElement => {
  const router = useRouter()
  const movieId = router?.query?.movieId as string

  const [name, setName] = useState<string>('')
  const [description, setDescription] = useState<string>('')
  const [year, setYear] = useState<string>('')

  const queryVariables: MovieArgs = {
    _id: movieId
  }

  const { data, loading } = useQuery(query, {
    skip: !process.browser || !movieId,
    variables: queryVariables,
    onCompleted: (e: { movie: Movie; user: User }) => {
      setName(e.movie?.name)
      setDescription(e.movie?.description)
      setYear(String(e.movie?.year || ''))
    }
  })

  const user: User = data?.user

  const movieAuthorization: UserAuthorization = user?.userAuthorizations?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.MOVIE
  })

  const updateVariables: UpdateMovieArgs = {
    _id: movieId,
    name,
    description,
    year: Number(year)
  }

  const [updateMovie, mutationState] = useMutation(mutation, {
    variables: updateVariables,
    onCompleted: (): void => {
      global.setNotification('success', 'Update successful')
    },
    onError: (e: ApolloError): void => {
      global.setNotification('error', e.message)
    }
  })

  const disableSubmit = mutationState.loading || !name

  return movieAuthorization?.write ? (
    <form
      onSubmit={(e): void => {
        e.preventDefault()
      }}
    >
      <CardContainer
        title={'Update movie'}
        loading={loading || mutationState.loading}
        content={
          <>
            <TextField
              fullWidth
              size={'small'}
              margin={'dense'}
              disabled={loading || mutationState.loading}
              label={'Name'}
              variant={'outlined'}
              value={name}
              onChange={(e): void => {
                setName(e.target.value)
              }}
            />
            <TextField
              fullWidth
              size={'small'}
              margin={'dense'}
              disabled={loading || mutationState.loading}
              label={'Description'}
              variant={'outlined'}
              value={description}
              onChange={(e): void => {
                setDescription(e.target.value)
              }}
            />
            <TextField
              fullWidth
              size={'small'}
              margin={'dense'}
              disabled={mutationState.loading}
              label={'Year'}
              variant={'outlined'}
              type={'number'}
              value={year}
              onChange={(e): void => {
                setYear(e.target.value)
              }}
            />
          </>
        }
        actions={
          <Button
            type={'submit'}
            size={'small'}
            color={'primary'}
            variant={'contained'}
            onClick={(): void => {
              updateMovie()
            }}
            disabled={disableSubmit}
          >
            {'Update'}
          </Button>
        }
      />
    </form>
  ) : null
}

export default MoviesUpdate
