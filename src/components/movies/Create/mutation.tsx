import { gql } from '@apollo/client'

export default gql`
  mutation($name: String!, $description: String, $year: Int) {
    movie: create_movie(name: $name, description: $description, year: $year) {
      _id
      name
      description
      year
    }
  }
`
