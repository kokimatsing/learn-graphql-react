import { ReactElement, useEffect } from 'react'

const SetCoordinates = (): ReactElement => {
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((pos): void => {
      global.lat = pos.coords.latitude
      global.lng = pos.coords.longitude
    })
  })

  return null
}

export default SetCoordinates
