import React, { ReactElement } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Button, Dialog, DialogContent } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  image: {
    display: 'block',
    margin: 'auto',
    maxWidth: theme.spacing(120),
    marginBottom: theme.spacing()
  },
  marginBottom: {
    marginBottom: theme.spacing()
  }
}))

const ImageViewer = ({
  open,
  onClose,
  imageUrl
}: {
  open: boolean
  onClose: VoidFunction
  imageUrl: string | null
}): ReactElement => {
  const classes = useStyles({})

  return (
    <>
      <Dialog open={Boolean(open)} onClose={onClose} maxWidth={'lg'}>
        <DialogContent>
          <img className={classes.image} src={imageUrl} />
        </DialogContent>
        <DialogContent>
          <Button onClick={onClose}>{'Close'}</Button>
        </DialogContent>
      </Dialog>
    </>
  )
}

export default ImageViewer
