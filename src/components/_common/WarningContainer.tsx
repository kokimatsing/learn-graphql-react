import React, { ReactElement } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import red from '@material-ui/core/colors/red'

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: theme.spacing(64),
    marginBottom: theme.spacing(),
    backgroundColor: red[900]
  }
}))

const WarningContainer = ({ title, subheader }: { title?: string; subheader?: string }): ReactElement => {
  const classes = useStyles({})

  return (
    <Card className={classes.root} variant={'outlined'} elevation={0}>
      <CardHeader
        title={title}
        titleTypographyProps={{
          style: { color: '#fff' }
        }}
        subheaderTypographyProps={{
          style: { color: '#fff' }
        }}
        subheader={subheader}
      />
    </Card>
  )
}

export default WarningContainer
