import React, { ReactElement, useState, useEffect } from 'react'
import { useTheme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/core/styles'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import Collapse from '@material-ui/core/Collapse'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import IconButton from '@material-ui/core/IconButton'
import FlipCameraAndroidIcon from '@material-ui/icons/FlipCameraAndroid'
import CloseIcon from '@material-ui/icons/Close'

// QrReader import is like this because of SSR issues.
// Read: https://github.com/JodusNodus/react-qr-reader/issues/91
// For more information, ask Weston.

let QrReader = null

if (process.browser) {
  QrReader = require('react-qr-reader')
}

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    top: theme.spacing(),
    right: theme.spacing()
  },
  grow: {
    height: '100%'
  }
}))

const QRCodeReader = ({
  open,
  onClose,
  onCapture
}: {
  open: boolean
  onClose: () => void
  onCapture: (string: string) => void
}): ReactElement => {
  const classes = useStyles({})
  const theme = useTheme()
  const [facingMode, setFacingMode] = useState('environment')
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    setLoaded(false)
  }, [open])

  return (
    <Dialog open={open} onClose={onClose} maxWidth={'xl'} fullScreen={useMediaQuery(theme.breakpoints.down('sm'))}>
      <DialogTitle>
        {'Scan QR code'}
        <IconButton onClick={onClose} className={classes.closeButton}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      {process.browser && window && (
        <Collapse in={loaded}>
          <QrReader
            facingMode={facingMode}
            style={{
              width: 300
            }}
            delay={500}
            onScan={onCapture}
            onLoad={(): void => {
              setLoaded(true)
            }}
            onError={(): void => null}
          />
        </Collapse>
      )}
      <div className={classes.grow} />
      <DialogActions id={'cameraActions'}>
        <IconButton
          size={'small'}
          onClick={() => {
            if (facingMode === 'environment') {
              setFacingMode('user')
            } else {
              setFacingMode('environment')
            }
          }}
        >
          <FlipCameraAndroidIcon />
        </IconButton>
      </DialogActions>
    </Dialog>
  )
}

export default QRCodeReader
