import React, { Dispatch, ReactElement, SetStateAction } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2)
  }
}))

const PageTabs = ({
  tab,
  setTab,
  tabs
}: {
  tab: string
  setTab: Dispatch<SetStateAction<string>>
  tabs: Array<{ label: string; value: string }>
}): ReactElement => {
  const classes = useStyles({})

  return (
    <Tabs
      variant={'scrollable'}
      className={classes.root}
      value={tab}
      indicatorColor={'primary'}
      textColor={'primary'}
      onChange={(e, v: string): void => {
        setTab(v)
      }}
    >
      {tabs.map((e) => (
        <Tab key={e.value} label={e.label} value={e.value} wrapped />
      ))}
    </Tabs>
  )
}

export default PageTabs
