import React, { ReactElement } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'

import Head from 'next/head'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2)
  }
}))

const PageTitle = ({
  title,
  subtitle,
  backButton
}: {
  title: string
  subtitle?: string
  backButton?: boolean
}): ReactElement => {
  const classes = useStyles({})
  const router = useRouter()

  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      {backButton && (
        <Button
          size={'small'}
          className={classes.root}
          variant={'outlined'}
          onClick={() => {
            router.back()
          }}
        >
          {'Back'}
        </Button>
      )}
      <Typography variant={'h4'} gutterBottom>
        {title}
      </Typography>
      <Typography variant={'h5'} gutterBottom>
        {subtitle}
      </Typography>
      <div className={classes.root} />
    </>
  )
}

export default PageTitle
