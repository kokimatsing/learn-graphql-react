import React, { ReactElement } from 'react'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import { makeStyles } from '@material-ui/core/styles'
import withWidth from '@material-ui/core/withWidth'
import classNames from 'classnames'

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(),
    cursor: 'pointer',
    borderRadius: theme.spacing(),
    border: `${theme.spacing(0.25)}px solid transparent`
  },
  hover: {
    '&:hover': {
      border: `${theme.spacing(0.25)}px solid ${theme.palette.divider}`
    }
  }
}))

const GridListContainer = ({
  width,
  children,
  initialCols = 5,
  disableHover,
  spacing,
  xl,
  lg,
  md,
  sm,
  xs
}: {
  width: string
  children: ReactElement | ReactElement[]
  initialCols?: number
  disableHover?: boolean
  spacing?: number
  xl?: number
  lg?: number
  md?: number
  sm?: number
  xs?: number
}): ReactElement => {
  const classes = useStyles({})

  const returnCols = (): number => {
    if (width === 'xl') {
      return xl || initialCols
    }

    if (width === 'lg') {
      return lg || initialCols
    }

    if (width === 'md') {
      return md || Math.max(initialCols - 1, 1)
    }

    if (width == 'sm') {
      return sm || Math.max(initialCols - 2, 1)
    }

    if (width === 'xs') {
      return xs || Math.max(initialCols - 3, 1)
    }

    return 1
  }

  if (!Array.isArray(children)) {
    return (
      <GridList cellHeight={'auto'} spacing={spacing || 0}>
        <GridListTile className={disableHover ? classes.root : classNames(classes.root, classes.hover)}>
          {children}
        </GridListTile>
      </GridList>
    )
  }

  return (
    <GridList cols={returnCols()} cellHeight={'auto'} spacing={spacing || 0}>
      {children
        .filter((e) => e)
        .map((element, index) => (
          <GridListTile key={index} className={disableHover ? classes.root : classNames(classes.root, classes.hover)}>
            {element}
          </GridListTile>
        ))}
    </GridList>
  )
}

export default withWidth()(GridListContainer)
