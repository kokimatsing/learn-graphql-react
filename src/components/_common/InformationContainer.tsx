import React, { ReactElement } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import Collapse from '@material-ui/core/Collapse'
import LinearProgress from '@material-ui/core/LinearProgress'
import orange from '@material-ui/core/colors/orange'

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: theme.spacing(64),
    marginBottom: theme.spacing(),
    backgroundColor: orange[500],
    cursor: 'pointer'
  }
}))

const InformationContainer = ({
  avatar = null,
  title,
  subheader,
  loading = false,
  onClick = null
}: {
  avatar?: ReactElement
  title?: string
  subheader?: string
  loading?: boolean
  onClick?: () => void
}): ReactElement => {
  const classes = useStyles({})

  return (
    <Card className={classes.root} variant={'outlined'} elevation={0} onClick={onClick}>
      <Collapse in={loading}>
        <LinearProgress />
      </Collapse>
      <CardHeader
        avatar={avatar}
        title={title}
        titleTypographyProps={{
          style: { color: '#fff' }
        }}
        subheaderTypographyProps={{
          style: { color: '#fff' }
        }}
        subheader={subheader}
      />
    </Card>
  )
}

export default InformationContainer
