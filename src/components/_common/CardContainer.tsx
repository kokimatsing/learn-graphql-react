import React, { ReactElement } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Collapse from '@material-ui/core/Collapse'
import LinearProgress from '@material-ui/core/LinearProgress'

import classnames from 'classnames'

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing()
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  borderSecondary: {
    borderLeft: `${theme.spacing()}px solid ${theme.palette.secondary.dark}`
  },
  borderPrimaryTop: {
    borderTop: `${theme.spacing()}px solid ${theme.palette.primary.dark}`
  },
  borderSecondaryTop: {
    borderTop: `${theme.spacing()}px solid ${theme.palette.secondary.dark}`
  }
}))

const CardContainer = ({
  avatar,
  title,
  imageUrl,
  subheader,
  content,
  children,
  actions,
  headerAction,
  loading = false,
  borderSecondary = false,
  borderPrimaryTop = false,
  borderSecondaryTop = false
}: {
  avatar?: ReactElement
  title?: string
  imageUrl?: string
  subheader?: string
  content?: ReactElement
  children?: ReactElement | ReactElement[]
  actions?: ReactElement
  headerAction?: ReactElement
  loading?: boolean
  borderSecondary?: boolean
  borderPrimaryTop?: boolean
  borderSecondaryTop?: boolean
}): ReactElement => {
  const classes = useStyles({})

  return (
    <Card
      className={classnames(
        classes.root,
        borderSecondary ? classes.borderSecondary : '',
        borderPrimaryTop ? classes.borderPrimaryTop : '',
        borderSecondaryTop ? classes.borderSecondaryTop : ''
      )}
      variant={'outlined'}
      elevation={0}
    >
      <Collapse in={loading}>
        <LinearProgress />
      </Collapse>
      {(avatar || title || subheader || headerAction) && (
        <CardHeader avatar={avatar} title={title} subheader={subheader} action={headerAction} />
      )}
      {imageUrl && <CardMedia className={classes.media} image={imageUrl} title={'Image'} />}
      {(content || children) && (
        <CardContent>
          {content}
          {children}
        </CardContent>
      )}
      {actions && <CardActions>{actions}</CardActions>}
    </Card>
  )
}

export default CardContainer
