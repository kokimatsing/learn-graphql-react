import React, { ReactElement, useState } from 'react'
import { useRouter } from 'next/router'

import CardContainer from 'src/components/_common/CardContainer'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import { useQuery, useMutation, ApolloError } from '@apollo/client'
import mutation from './mutation'
import query from './query'

import { DeleteCategoryArgs } from '../../../_types/categories'
import { User } from '../../../_types/users'
import { UserAuthorization } from '../../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../../_types/enums/userAuthorizationScope'

const CategoriesDelete = (): ReactElement => {
  const router = useRouter()
  const categoryId = router?.query?.categoryId as string

  const [checked, setChecked] = useState<boolean>(false)

  const queryVariables: DeleteCategoryArgs = {
    _id: categoryId
  }

  const { data, loading } = useQuery(query, {
    skip: !process.browser || !categoryId,
    variables: queryVariables
  })

  const user: User = data?.user

  const categoryAuthorization: UserAuthorization = user?.userAuthorizations?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.CATEGORY
  })

  const deleteVariables: DeleteCategoryArgs = {
    _id: categoryId
  }

  const [deleteTemplate, mutationState] = useMutation(mutation, {
    variables: deleteVariables,
    onCompleted: (): void => {
      global.setNotification('success', 'Delete successful')
      router.push('/categories', '/categories')
    },
    onError: (e: ApolloError): void => {
      global.setNotification('error', e.message)
    }
  })

  return categoryAuthorization?.destroy ? (
    <>
      <form
        onSubmit={(e): void => {
          e.preventDefault()
        }}
      >
        <CardContainer
          title={'Delete'}
          loading={loading || mutationState.loading}
          content={
            <>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checked}
                    onChange={(): void => {
                      setChecked(!checked)
                    }}
                    color={'primary'}
                  />
                }
                label={'Confirm I want to delete this category'}
              />
            </>
          }
          actions={
            <Button
              type={'submit'}
              size={'small'}
              color={'primary'}
              variant={'contained'}
              disabled={!checked}
              onClick={(): void => {
                deleteTemplate()
              }}
            >
              {'Delete'}
            </Button>
          }
        />
      </form>
    </>
  ) : null
}

export default CategoriesDelete
