import { gql } from '@apollo/client'

export default gql`
  mutation(
    $_id: ID!
  ) {
    category: delete_category(
      _id: $_id
    ) {
      _id
    }
  }
`
