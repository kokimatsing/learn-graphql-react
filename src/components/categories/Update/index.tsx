import React, { ReactElement, useState } from 'react'
import { useRouter } from 'next/router'

import CardContainer from 'src/components/_common/CardContainer'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import { useMutation, useQuery, ApolloError } from '@apollo/client'
import mutation from './mutation'
import query from './query'

import { Category, CategoryArgs, UpdateCategoryArgs } from '../../../_types/categories'
import { User } from '../../../_types/users'
import { UserAuthorization } from '../../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../../_types/enums/userAuthorizationScope'

const CategoriesUpdate = (): ReactElement => {
  const router = useRouter()
  const categoryId = router?.query?.categoryId as string

  const [name, setName] = useState<string>('')
  const [description, setDescription] = useState<string>('')

  const queryVariables: CategoryArgs = {
    _id: categoryId
  }

  const { data, loading } = useQuery(query, {
    skip: !process.browser || !categoryId,
    variables: queryVariables,
    onCompleted: (e: { category: Category; user: User }) => {
      setName(e.category?.name)
      setDescription(e.category?.description)
    }
  })

  const user: User = data?.user

  const categoryAuthorization: UserAuthorization = user?.userAuthorizations?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.CATEGORY
  })

  const updateVariables: UpdateCategoryArgs = {
    _id: categoryId,
    name,
    description
  }

  const [updateCategory, mutationState] = useMutation(mutation, {
    variables: updateVariables,
    onCompleted: (): void => {
      global.setNotification('success', 'Update successful')
    },
    onError: (e: ApolloError): void => {
      global.setNotification('error', e.message)
    }
  })

  const disableSubmit = mutationState.loading || !name

  return categoryAuthorization?.write ? (
    <form
      onSubmit={(e): void => {
        e.preventDefault()
      }}
    >
      <CardContainer
        title={'Update category'}
        loading={loading || mutationState.loading}
        content={
          <>
            <TextField
              fullWidth
              size={'small'}
              margin={'dense'}
              disabled={loading || mutationState.loading}
              label={'Name'}
              variant={'outlined'}
              value={name}
              onChange={(e): void => {
                setName(e.target.value)
              }}
            />
            <TextField
              fullWidth
              size={'small'}
              margin={'dense'}
              disabled={loading || mutationState.loading}
              label={'Description'}
              variant={'outlined'}
              value={description}
              onChange={(e): void => {
                setDescription(e.target.value)
              }}
            />
          </>
        }
        actions={
          <Button
            type={'submit'}
            size={'small'}
            color={'primary'}
            variant={'contained'}
            onClick={(): void => {
              updateCategory()
            }}
            disabled={disableSubmit}
          >
            {'Update'}
          </Button>
        }
      />
    </form>
  ) : null
}

export default CategoriesUpdate
