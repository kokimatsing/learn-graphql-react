import { gql } from '@apollo/client'

export default gql`
  mutation(
    $_id: ID!
    $name: String
    $description: String
  ) {
    category: update_category(
      _id: $_id
      name: $name
      description: $description
    ) {
      _id
      name
      description
    }
  }
`
