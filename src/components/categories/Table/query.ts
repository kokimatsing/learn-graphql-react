import { gql } from '@apollo/client'

export default gql`
  query(
    $limit: Int!
    $searchText: String
    $skip: Int!
    $sortBy: String
    $sortDirection: SortDirectionType
  ) {
    categories: categories(
      limit: $limit
      searchText: $searchText
      skip: $skip
      sortBy: $sortBy
      sortDirection: $sortDirection
    ) {
      _id
      name
      description
    }

    categoryCount: category_count(
      searchText: $searchText
    )
  }
`
