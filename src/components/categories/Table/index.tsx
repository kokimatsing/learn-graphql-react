import React, { ReactElement } from 'react'
import { useRouter } from 'next/router'

import Table from '@material-ui/core/Table'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableContainer from '@material-ui/core/TableContainer'
import LinearProgress from '@material-ui/core/LinearProgress'
import IconButton from '@material-ui/core/IconButton'
import LaunchIcon from '@material-ui/icons/Launch'

import { useQuery } from '@apollo/client'
import query from './query'

import { Category, CategoriesArgs } from '../../../_types/categories'

const CategoriesTable = (): ReactElement => {
  const router = useRouter()

  const initialVariables: CategoriesArgs = {
    skip: 0,
    limit: 100
  }

  const { data, loading } = useQuery(query, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
    partialRefetch: true,
    returnPartialData: true,
    skip: !process.browser,
    ssr: false,
    variables: initialVariables
  })

  const categories = data?.categories || []

  return (
    <>
      {loading && <LinearProgress />}
      <TableContainer>
        <Table size={'small'}>
          <TableHead>
            <TableRow selected>
              <TableCell padding={'checkbox'} />
              <TableCell>{'Name'}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {categories.map(
              (category: Category, index: number): ReactElement => {
                return (
                  <TableRow key={category._id} selected={index % 2 !== 0} hover>
                    <TableCell padding={'checkbox'}>
                      <IconButton
                        onClick={(): void => {
                          router.push('/categories/[categoryId]', `/categories/${category._id}`)
                        }}
                      >
                        <LaunchIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell>{category?.name}</TableCell>
                  </TableRow>
                )
              }
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}

export default CategoriesTable
