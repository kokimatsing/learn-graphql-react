import React, { ReactElement, useState } from 'react'
import { useRouter } from 'next/router'

import CardContainer from 'src/components/_common/CardContainer'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import { useMutation, ApolloError, useQuery } from '@apollo/client'
import mutation from './mutation'
import query from './query'

import { CreateCategoryArgs } from '../../../_types/categories'
import { User } from '../../../_types/users'
import { UserAuthorization } from '../../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../../_types/enums/userAuthorizationScope'

const CategoriesCreate = (): ReactElement => {
  const router = useRouter()

  const [name, setName] = useState<string>('')
  const [description, setDescription] = useState<string>('')

  const createVariables: CreateCategoryArgs = {
    name,
    description
  }

  const [createCategory, mutationState] = useMutation(mutation, {
    variables: createVariables,
    onCompleted: (e): void => {
      global.setNotification('success', 'Create successful')
      router.push('/categories/[categoryId]', `/categories/${e?.category?._id}`)
    },
    onError: (e: ApolloError): void => {
      global.setNotification('error', e.message)
    }
  })

  const disableSubmit = mutationState.loading || !name

  const { data } = useQuery(query, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
    partialRefetch: true,
    returnPartialData: true,
    ssr: false
  })

  const user: User = data?.user

  const categoryAuthorization: UserAuthorization = user?.userAuthorizations?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.CATEGORY
  })

  if (!categoryAuthorization?.write) {
    return null
  }

  return (
    <form
      onSubmit={(e): void => {
        e.preventDefault()
      }}
    >
      <CardContainer
        title={'Create category'}
        loading={mutationState.loading}
        content={
          <>
            <TextField
              fullWidth
              size={'small'}
              margin={'dense'}
              disabled={mutationState.loading}
              label={'Name'}
              variant={'outlined'}
              value={name}
              onChange={(e): void => {
                setName(e.target.value)
              }}
            />
            <TextField
              fullWidth
              size={'small'}
              margin={'dense'}
              disabled={mutationState.loading}
              label={'Description'}
              variant={'outlined'}
              type={'textarea'}
              value={description}
              onChange={(e): void => {
                setDescription(e.target.value)
              }}
            />
          </>
        }
        actions={
          <Button
            type={'submit'}
            size={'small'}
            color={'primary'}
            variant={'contained'}
            onClick={(): void => {
              createCategory()
            }}
            disabled={disableSubmit}
          >
            {'Create'}
          </Button>
        }
      />
    </form>
  )
}

export default CategoriesCreate
