import { gql } from '@apollo/client'

export default gql`
  mutation(
    $name: String!
    $description: String
  ) {
    category: create_category(
      name: $name
      description: $description
    ) {
      _id
      name
      description
    }
  }
`
