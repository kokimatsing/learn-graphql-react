import { createMuiTheme } from '@material-ui/core'

export default createMuiTheme(
  {
    palette: {
      primary: {
        main:
          '#1665FF'
      },
      secondary: {
        main:
          '#002a7d'
      },
      error: {
        main:
          '#CC6600'
      }
    }
  }
)
