import { createMuiTheme } from '@material-ui/core'

export default createMuiTheme(
  {
    palette: {
      background: {
        paper:
          '#212121',
        default:
          '#333333'
      },
      type: 'dark',
      primary: {
        main:
          '#1665FF'
      },
      secondary: {
        main:
          '#002a7d'
      },
      error: {
        main:
          '#CC6600'
      }
    }
  }
)
