export enum UserAuthorizationScope {
  CATEGORY = 'CATEGORY',
  MOVIE = 'MOVIE'
}
