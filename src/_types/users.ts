import { ObjectId } from 'mongodb'
import { UserAuthorization } from './userAuthorization'

export interface User {
  __typename?: 'User'
  _id?: ObjectId
  email?: string
  name?: string
  password?: string
  userAuthorizations?: UserAuthorization[]
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date
  deleted?: boolean
}
export interface UserArgs {
  _id?: ObjectId
}
export interface CreateUserArgs {
  email?: string
  name?: string
  password?: string
}
export interface SignInUserArgs {
  email: string
  password: string
}
export interface UserToken {
  _id?: string
  name?: string
  email?: string
  authToken?: string
}
export interface AuthToken {
  userSessionId: string
}
