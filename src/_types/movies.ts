import { ObjectId } from 'mongodb'
import { SortDirectionType } from './enums/sortDirectionType'
import { Category } from './categories'

export interface Movie {
  __typename?: 'Movie'
  _id?: ObjectId
  name?: string
  description?: string
  year?: number
  categoryId?: ObjectId
  category?: Category
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date
  deleted?: boolean
}
export interface MoviesArgs {
  limit: number
  searchText?: string
  categoryId?: string
  year?: number
  skip: number
  sortBy?: string
  sortDirection?: SortDirectionType
}
export interface MovieArgs {
  _id: string
}
export interface CreateMovieArgs {
  name?: string
  year?: number
  description?: string
  categoryId?: string
}
export interface UpdateMovieArgs {
  _id?: string
  name?: string
  year?: number
  description?: string
  categoryId?: string
}
export interface DeleteMovieArgs {
  _id: string
}
