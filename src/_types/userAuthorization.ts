import { UserAuthorizationScope } from './enums/userAuthorizationScope'
import { ObjectId } from 'mongodb'
import { SortDirectionType } from './enums/sortDirectionType'
import { User } from './users'

export interface UserAuthorization {
  __typename?: 'UserAuthorization'
  _id?: ObjectId
  user?: User
  userId?: ObjectId
  scope?: UserAuthorizationScope
  read?: boolean
  write?: boolean
  destroy?: boolean
  createdAt?: Date
  updatedAt?: Date
}

export interface UserAuthorizationsArgs {
  userId?: string
  limit: number
  searchText?: string
  skip: number
  sortBy?: string
  sortDirection?: SortDirectionType
}

export interface UserAuthorizationArgs {
  _id: string
}

export interface ToggleUserAuthorizationArgs {
  scope?: UserAuthorizationScope
  userId?: string
  read?: boolean
  write?: boolean
  destroy?: boolean
}
