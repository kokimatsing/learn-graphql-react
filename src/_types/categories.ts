import { ObjectId } from 'mongodb'
import { SortDirectionType } from './enums/sortDirectionType'
import { Movie } from './movies'

export interface Category {
  __typename?: 'Category'
  _id?: ObjectId
  name?: string
  description?: string
  movies?: Movie[]
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date
  deleted?: boolean
}
export interface CategoriesArgs {
  limit: number
  searchText?: string
  skip: number
  sortBy?: string
  sortDirection?: SortDirectionType
}
export interface CategoryArgs {
  _id: string
}
export interface CreateCategoryArgs {
  name?: string
  description?: string
}
export interface UpdateCategoryArgs {
  _id?: string
  name?: string
  description?: string
}
export interface DeleteCategoryArgs {
  _id: string
}
