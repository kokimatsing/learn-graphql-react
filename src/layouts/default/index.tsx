import React, { ReactElement, FunctionComponent } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import MyAppBar from './MyAppBar'
import { useQuery } from '@apollo/client'
import query from './query'

import { User } from '../../_types/users'

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: theme.spacing(196),
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
    padding: theme.spacing(),
    margin: 'auto'
  }
}))

const layoutsHome = (Page: FunctionComponent) => (): ReactElement => {
  const classes = useStyles({})

  const { data } = useQuery(query, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
    partialRefetch: true,
    returnPartialData: true,
    ssr: false
  })

  const user: User = data?.user

  return (
    <>
      <MyAppBar user={user} />
      <div className={classes.root}>
        <Page />
      </div>
    </>
  )
}

export default layoutsHome
