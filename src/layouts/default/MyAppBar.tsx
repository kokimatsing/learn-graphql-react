import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'
import { useTheme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import React, { ReactElement } from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Avatar from '@material-ui/core/Avatar'
import { User } from '../../_types/users'
import PersonIcon from '@material-ui/icons/Person'

import { UserAuthorization } from '../../_types/userAuthorization'
import { UserAuthorizationScope } from '../../_types/enums/userAuthorizationScope'

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1
  },
  marginRight: {
    marginRight: theme.spacing()
  },
  title: {
    cursor: 'pointer',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'clip',
    color: '#fff'
  },
  logo: {
    height: theme.spacing(4),
    width: theme.spacing(4),
    marginRight: theme.spacing()
  }
}))

const MyAppBar = ({ user }: { user: User }): ReactElement => {
  const classes = useStyles({})
  const router = useRouter()
  const theme = useTheme()

  const movieAuthorization: UserAuthorization = user?.userAuthorizations?.find((userAuthorization) => {
    return userAuthorization.scope == UserAuthorizationScope.MOVIE
  })

  return (
    <AppBar elevation={0} position={useMediaQuery(theme.breakpoints.up('md')) ? 'sticky' : 'relative'}>
      <Toolbar>
        <img alt={'Learn GQL'} src={'/logo.svg'} className={classes.logo} />
        <div className={classes.grow} />

        {movieAuthorization?.write && user?.name && (
          <Avatar
            className={classes.marginRight}
            variant={'rounded'}
            onClick={async (): Promise<void> => {
              router.push('/movies/create')
            }}
          >
            <PersonIcon />
          </Avatar>
        )}

        {user?.name && <Avatar variant={'rounded'}>{user?.name[0]}</Avatar>}
        {!user?.name && (
          <Avatar
            variant={'rounded'}
            onClick={async (): Promise<void> => {
              router.push('/sign-in')
            }}
          >
            <PersonIcon />
          </Avatar>
        )}
      </Toolbar>
    </AppBar>
  )
}

export default MyAppBar
