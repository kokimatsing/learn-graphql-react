import { gql } from '@apollo/client'

export default gql`
  query {
    user: current_user {
      _id
      name
      userAuthorizations {
        scope
        read
        write
        destroy
      }
    }
  }
`
