import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  NormalizedCacheObject
} from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { BACKEND_BASE_URL } from './returnUrl'
import Cookies from 'js-cookie'

const authLink = setContext(
  (
    _,
    { headers }
  ) => {
    const accessToken = Cookies.get(
      'accessToken'
    )

    const deviceId =
      'abc_123'

    return {
      headers: {
        ...headers,
        accessToken,
        deviceId,
        latitude:
          global.lat,
        longitude:
          global.lng
      }
    }
  }
)

const link = createHttpLink(
  {
    uri: BACKEND_BASE_URL()
  }
)

const client: ApolloClient<NormalizedCacheObject> = new ApolloClient(
  {
    cache: new InMemoryCache(),
    link: authLink.concat(
      link
    ),
    name:
      'learn-react-qgl',
    version: '1.0'
  }
)

export default client
