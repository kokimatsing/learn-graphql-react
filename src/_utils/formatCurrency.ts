const formatCurrency = (
  amount: number
): string => {
  if (!amount) {
    return '₱0.00'
  }

  const sign =
    amount < 0
      ? '-'
      : ''

  return `${sign}₱${Math.abs(
    amount
  )
    .toFixed(2)
    .toString()
    .replace(
      /\B(?=(\d{3})+(?!\d))/g,
      ','
    )}`
}

export default formatCurrency
