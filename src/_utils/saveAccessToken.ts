import Cookies from 'js-cookie'

const saveAccessToken = (
  authToken: string
): void => {
  Cookies.set(
    'accessToken',
    authToken,
    {
      expires: 1
    }
  )
}

export default saveAccessToken
