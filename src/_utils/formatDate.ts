const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

const formatDate = (
  unformattedDate: Date
): string => {
  if (
    !unformattedDate
  ) {
    return ''
  }

  const date = new Date(
    unformattedDate
  )
  const month = date.getMonth()
  const day = date.getDate()
  const year = date.getFullYear()
  return `${months[month]} ${day}, ${year}`
}

export default formatDate
